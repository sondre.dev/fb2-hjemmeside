# Styret

Styret kan primært nås på e-post [styret@fb2.no](mailto:styret@fb2.no). Hvis du sender en melding til denne adressen, vil alle i styret motta eposten. Det betyr at du kan få raskere tilbakemelding enn hvis du sender eposten til bare en av medlemmene.

Styreleder
----------

<table>
<tr><td class="b">Navn</td><td class="a">Pål Hermunn Johansen (leil. 5011)</td></tr>
<tr><td class="a">Telefon</td><td class="b">415 45 896</td></tr>
<tr><td class="b">E-post</td><td class="a">hermunn@hermunn.com, paal@fb2.no</td></tr>
</table>

Styremedlemmer
--------------

<table>
<tr><td class="b">Navn</td><td class="a">Andrea Lovise K. Moe (leil. 6003)</td></tr>
<tr><td class="a">Telefon</td><td class="b">943 82 75</td></tr>
<tr><td class="b">E-post</td><td class="a">andrea@fb2.no</td></tr>
</table>

<table>
<tr><td class="a">Navn</td><td class="b">Amund Sigurdssønn Karlsen (leil. 4006)</td></tr>
<tr><td class="b">Telefon</td><td class="a">924 89 479</td></tr>
<tr><td class="a">E-post</td><td class="b">amund@fb2.no</td></tr>
</table>

<table>
<tr><td class="b">Navn</td><td class="a">Sondre Bø Kongsgård (leil. 1008)</td></tr>
<tr><td class="a">Telefon</td><td class="b">902 99 484</td></tr>
<tr><td class="b">E-post</td><td class="a">sondre@fb2.no</td></tr>
</table>

Varamedlemmer
-------------

Daniela Schäfer Olstad

Maren Lihagen
