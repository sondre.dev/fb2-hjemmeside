Nyttig informasjon
==================

I menyen til venstre finner du nyttig informasjon innnenfor mange områder. Vi anbefaler nye beboere, eiere og meglere å lese alle sidene, og så [kontakte styret](/sameiet/styret/) hvis noe er uklart.

