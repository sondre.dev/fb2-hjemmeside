Eierskifte
==========

Informer styret
---------------

Ved eierskifte plikter selger og kjøper skriftelig å informere [Styret](/styret), samt gi følgende opplysninger:

- selgers nye postadresse (selgers ansvar)
- kjøpers navn (selger og kjøpers ansvar)
- <span class="strike">kjøpers personnummer (selger og kjøpers ansvar)</strike>
- kjøpers e-post adresse (selger og kjøpers ansvar)
- kjøpers postadresse (selger og kjøpers ansvar)