Vaskeri

Vaskeriansvarlig
----------------

[Ta kontakt med styret](/styret/).

Vaskekort
---------

[Ta kontakt med styret](/styret/).

Feil / mangler
--------------

[Ta kontakt med styret](/styret/).

Bestille time
-------------

For å kunne benytte vaskeriet må du bestille time ved å henge opp en merket hengelås på tavla i vaskeriet. Hengelåsen skal merkes med Ieilighetsnummeret. 

Åpningstider
------------

<table>
<tr><td class="a">Mandag til fredag</td><td class="b">kl 08.00 - kl 22.00</td></tr>
<tr><td class="b">Lørdag</td><td class="a">kl 08.00 - kl 20.00</td></tr>
<tr><td class="a">Søn- og helligdager</td><td class="b">stengt</td></tr>
</table>

Følg instruks
-------------

Instruks for bruk av maskinene må følges nøye, det samme gjelder regler for rydding og rengjøring. Det er spesielt viktig å fjerne lo fra filter i tørketrommel på grunn av brannfare.

