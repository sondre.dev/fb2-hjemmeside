Lover
=====

Eierseksjonsloven
-----------------

Dine plikter og rettigheter er hjemlet i [Lov om eierseksjoner](https://lovdata.no/dokument/NL/lov/1997-05-23-31).

Se også [Wikipedia for mer informasjon om eierseksjoner](http://no.wikipedia.org/wiki/Eierseksjon).
