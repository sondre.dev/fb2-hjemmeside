# Kontakt

## Besøksadresse
Folke Bernadottes Vei 2<br>
0862 Oslo<br>
[Åpne kart i Google maps](https://www.google.com/maps/preview#!q=Folke+Bernadottes+Vei+2%2C+Oslo%2C+Norway).

## Fakturaadresse
Sameiet FB2<br>
Folke Bernadottes Vei 2<br>
0862 Oslo

## E-post
Send oss en epost på [styret@fb2.no](mailto:styret@fb2.no).

# Enkeltpersoner i Styret
For kontaktinformasjon til enkeltpersoner i styret, gå til [styresiden](/styret/).

## Håndverkere
For kontaktinformasjon om tilknyttede håndverkere, gå til [håndverksiden](/nyttig/handverkere/).

## Forretningsfører og revisor
For kontaktinformasjon til vår forretningsfører og revisor, [gå til infosiden om sameiet](/sameiet/).

## Annet
For annen kontaktinformasjon [gå til akuttsiden](/akkutt/).