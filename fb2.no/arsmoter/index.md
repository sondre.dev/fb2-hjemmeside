Årsmøter
========

Ordinært årsmøte/sameiemøte avholdes en gang i året, i april. Det kan også arrangeres ekstraordinære sameiemøter dersom det er nødvendig, som beskrevet i [vedtektene](/nyttig/vedtekter/).

I et sameiemøte vil kun saker fra innkallingen behandles. Derfor er det viktig at saker blir sendt inn i god tid før møtet. Om lag en måned før møtet skal holdes, vil det sendes ut en melding om dato for møtet og en frist for innkalling av saker for behandling i det gitte møtet. Dersom du trenger hjelp eller hint angående dette, ta [kontakt med styret](/kontakt/).

Undermenyen til venstre inneholder lenker for hvert årsmøte. Der kan du laste ned innkalling og referat for hvert møte tilbake til 2013.