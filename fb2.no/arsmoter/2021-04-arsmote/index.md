Ordinært sameiemøte april 2021
==============================

I april 2021 vil det holdes et ordinært sameiemøte, og per 1. mars ser det ut til at også dette møtet vil bli holdt på Zoom. Dato for møtet er ennå ikke bestemt.

Når datoen er bestemt vil det bli sendt ut et varsel om dette til alle sameierne. I dette varselet vil det også være en frist for innsending av saker. Etter at fristen har gått ut, vil styret gjøre klar en innkalling, og innkallingen sendes alltid ut mellom 8 og 20 dager før sameiemøtet. Kun saker i innkallingen kan behandles i sameiemøtet.

Dokumenter for sameiemøtet vil bli gjort tilgjengelig her. Gi beskjed om du trenger en utskrift av dokumentene.

